'use strict';

const express = require ('express');
//Para poder usa la función path que busca la ruta de un fichero.
const path = require('path');

// Para nuestras pruebas vamos a utilizar algunas librerias con validadores Chai
//la libreria mocha es un framework para realizar pruebas en javascrip


// Constantes
//Qué puerto queremos utilizar (8080 ya está ocupado)
const PORT = 8081;

//Aplicacion que escucha: 1 inicilizar el servidor (ya estará escuchando pero no hace nada)
const APP = express();

//para que no mire en node_modules en su lugar, en la carpeta raíz.
APP.use(express.static(__dirname));

//Qué quiero gestionar según lo que le pidan al servidor, por ejemplo qué quiero que haga con el get de la raiz
APP.get('/', function(req,res){
  //funtions siempre tiene dos objetos, peticion y respuesta.
    //res.send("Bienvenido a la web ÁRíder\n");

  //En lugar de devolver un mensaje como antes, vamos a devolver la propia web.
  //Se indica el fichero index.html que lo va a buscar con path y lo une con una variable
  //que tiene interna join con __dirname (se posiciona a la raiz de tu proyecto)+ el nombre del fichero.
  res.sendFile(path.join(__dirname+'/index.html'));
});

//Para que escuche en el puerto que hemos incluido en la variable
APP.listen(PORT);

console.log("Express funcionando en el puerto" + PORT);
