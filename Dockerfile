FROM node:boron

# Crear directorio de la aplicación, y no nos preocupamos de permisos porque están dentro del motor de Docker
RUN mkdir -p /usr/src/app

# Indicamos que la carpeta de trabajo es la que acabamos de crear.
WORKDIR /usr/src/app

# Instalar dependencias, copiando en la carpeta creada el json donde hemos puesto las dependencies
COPY package.json /usr/src/app

# Ejecutar para no lanzar a mano
RUN npm install


# Empaquetar código, copia todo lo que hay en el node_modules a nuestra carpeta.
# en la carpeta .dockerignore pongo las carpetas que no quiero que me copie, para que no sea pesada porque no las utilizo.
COPY . /usr/src/app

# Ponemos elpuerto donde exponer el contenedor que lo normal es utilizar el mismo del servidor.
EXPOSE 8081

#Para que se lance, el comando CMD (importante los espacios de CMD)
CMD [ "npm","start" ]
