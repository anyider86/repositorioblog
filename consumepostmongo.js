const URL = "https://api.mlab.com/api/1/databases/angela_mongodb/collections/posts?apiKey=dvi5e-mBiwFc7TIPRfAoNqZvcLm2L743";
var response;

function obtenerPosts(){
  //Java es sensible en la función XMLHttpRequest
  var peticion = new XMLHttpRequest();
  //Como funciona de manera asincrona hay que decirle qué hacer cuando devuelva datos.
  //Primero hago petición con el método Open, diciendole qué operación quiero hacer, para leer post sería un get.
  //También es necesaria la URL y opcional si funciona en modo asincrono (True) y sincrono(false)
  peticion.open("GET", URL, false);
  //Para decirle que se traiga los datos, si se puede, en un formato concreto:
  peticion.setRequestHeader("Content-Type", "application/json");
  //Para enviarlo a la URL que le he puesto. Es decir es como abrir el navegador, poner la dirección y darle a intro.
  peticion.send();
  //me guardo la respuesta en una variable (no estoy teniendo en cuenta que podría fallar)
  response = JSON.parse(peticion.responseText);
  //Que escriba por consola la respuesta para ver si esta ok
  console.log(response);


  //  Función que llamamos, y que va a mostrar el título por cda objeto que tengo en response
  mostrarPosts();
};

function mostrarPosts(){
  var tabla = document.getElementById("tablaPosts");
  for(var i=0; i<response.length; i++){
    //alert(response[i].titulo);

    //insertamos filas en la tabla creada.
    var fila = tabla.insertRow(i+1);
    var celdaTitulo = fila.insertCell(0);
    var celdaTexto= fila.insertCell(1);
    var celdaAutor = fila.insertCell(2);


    if(response[i].autor != undefined){
      celdaAutor.innerHTML = response[i].autor.nombre + " " +response[i].autor.apellido;
    }
    else{
      celdaAutor.innerHTML ="Anónimo";
    }

    celdaTexto.innerHTML = response[i].texto;
    celdaTitulo.innerHTML = response[i].titulo;
  }
}
