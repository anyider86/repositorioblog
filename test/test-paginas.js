// Utilizamos Chia, (referencias a librerias)
//una función que comprueba si dos valores son iguales.
var expect = require('chai').expect;

//Para poder hacer peticiones de URL, hacemos lo siguiente:
var request = require('request');

//Jquery tiene para recoger datos una palabra reservada que es $(), que permite buscar una serie de elementos:
  // 1.- Elementos que van directamente con # como por ejemplo el idedntificador.
    // #identificador = <xxx id ="identificador"
    //elemento = <elemento .....
    // .clase = <xxx class = "clase"...
      //ejemplo si tenemso <table id ="prueba" class ="clase1"... ponemos $("table#prueba.clase1")
var $ = require('chai-jquery');

//Creación de conjunto de pruebas, para que no salgan todas juntas, metemos las que queremos empaquetar en un describe:
describe("Pruebas sencillas", function() {
  //Definimos la primera prueba
  it('Test suma', function(){
    expect(9+4).to.equal(13);
    //console.log("Prueba completada");
  });
});

describe("Pruebas de red", function(){
  //Prueba de verificación de entrada a una web. Asíncrono
    //En function ponemos un nombre, por convenio done.
    //hacemos un request que esta preparado para peticiones a web, el body te lo devuelve en un parametro, el response en otro...
  it('Test internet',function(done){
    request.get("http://www.forocoches.com",function(error, response, body){
      //ponemos lo que esperamos recibir de la web
      expect(response.statusCode).to.equal(200)||expect(response.statusCode).to.equal(304);
      //ponemos el done para que vuelva al inicio, si no, no termina la prueba
      done();
    });
  });

  it('Test Ejercicio1 mi localhost',function(done){
    request.get("http://localhost:8082",function(error, response, body){
      //ponemos lo que esperamos recibir de la web
      expect(response.statusCode).to.equal(200)||expect(response.statusCode).to.equal(304);
      //ponemos el done para que vuelva al inicio, si no, no termina la prueba
      done();
    });
  });


  it('Test Ejercicio2 body',function(done){
    request.get("http://localhost:8082",function(error, response, body){
      //ponemos lo que esperamos recibir del body
      //Igual necesitamos meter expresiones regulares.
      expect(body).contains("<h1>Bienvenido a mi blog</h1>");
      //ponemos el done para que vuelva al inicio, si no, no termina la prueba
      done();
    });
  });

});

describe ("Test contenido html", function(done){
  it("Test H1", function() {
    request.get("http://localhost:8082",function(error, response, body){
      expect($('h1')).to.have.text("Bienvenido a mi blog");
      done();
    });
  });

  it("Test H2", function() {
    request.get("http://localhost:8082",function(error, response, body){
      expect($('h2')).to.have.text("Construcción");
      done();
    });
  });
});
